package com.example.jeyun_b.detectheading_proto;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class MainActivity extends Activity implements SensorEventListener,
        OnChartValueSelectedListener {

    int current_state = 0; // calibration - DRmode / 0 - 1 - 2
    int head_mag = 0; // circular, head == rear : 비었다/ head + 1 == rear : 가득 찼다
    int rear_mag = 0;
    int head_noise = 0;
    int rear_noise = 0; // 사실상 rear는 쓰지 않음
    int qsize = 50;
    float[] mag_Q;
    float[] noise_Q;
    boolean isFull_mag_Q = false;
    boolean isFull_noise_Q = false;

    TextView textView_state;
    TextView textView_x;
    TextView textView_z;
    TextView textView_fx;
    TextView textView_fz;

    private SensorManager sensorManager_mag;
    private Sensor sensor_mag;

    //for kalman 초기화
    float m_stateEstimate_x = 0.0f; //초기값 문제
    float m_estimateCovariance_x = 1.0f;

    float noise_stateEstimate_x = 2.0f;  //잡음은 정규분포를 따른다 (0, 1)
    float noise_estimaeCovariance_x = 3.0f;

    private LineChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.d("dd","main");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mag_Q = new float[qsize];
        noise_Q = new float[qsize];

        textView_state = (TextView) findViewById(R.id.textView_state);
        textView_x = (TextView) findViewById(R.id.textView_x);
        textView_z = (TextView) findViewById(R.id.textView_z);
        textView_fx = (TextView) findViewById(R.id.textView_fx);
        textView_fz = (TextView) findViewById(R.id.textView_fz);

        sensorManager_mag = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor_mag = sensorManager_mag.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        if (sensorManager_mag.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null) {
            textView_x.setText("자기 센서가 없어요.");
        }

        mChart = (LineChart) findViewById(R.id.chart);
        mChart.setOnChartValueSelectedListener(this);

        // enable description text
        mChart.getDescription().setEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);

        // set an alternative background color
        mChart.setBackgroundColor(Color.LTGRAY);

        List<Entry> magX = new ArrayList<Entry>();
        List<Entry> filteredX = new ArrayList<Entry>();
        List<Entry> filteredX2 = new ArrayList<Entry>();

        // temporary  initialize
        magX.add(new Entry(0,0.0f));
        filteredX.add(new Entry(0,0.0f));
        filteredX2.add(new Entry(0,0.0f));

        LineDataSet setComp1 = new LineDataSet(magX, "magX");
        setComp1.setValueTextColor(Color.RED);
        setComp1.setColor(Color.RED);
        setComp1.setCircleColor(Color.RED);
        //setComp1.setAxisDependency(AxisDependency.LEFT);
        LineDataSet setComp2 = new LineDataSet(filteredX, "filteredX");
        setComp2.setValueTextColor(Color.GREEN);
        setComp2.setColor(Color.GREEN);
        setComp2.setCircleColor(Color.GREEN);
        //setComp2.setAxisDependency(AxisDependency.LEFT);
        LineDataSet setComp3 = new LineDataSet(filteredX2, "filteredX2");
        setComp2.setValueTextColor(Color.GRAY);
        setComp2.setColor(Color.GRAY);
        setComp2.setCircleColor(Color.GRAY);

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(setComp1);
        dataSets.add(setComp2);
        dataSets.add(setComp3);

        LineData data = new LineData(dataSets);
        data.setValueTextColor(Color.WHITE);

        // add empty data
        mChart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        //l.setTypeface(mTfLight);
        l.setTextColor(Color.WHITE);

        XAxis xl = mChart.getXAxis();
        //xl.setTypeface(mTfLight);
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis leftAxis = mChart.getAxisLeft();
        //leftAxis.setTypeface(mTfLight);
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(50f);
        leftAxis.setAxisMinimum(-50f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        magnetic = new float[3];
        q_mag_x = new float[QSIZE];
        q_mag_z = new float[QSIZE];
        q_noise_x = new float[QSIZE];
        q_noise_z = new float[QSIZE];
    }

    @Override
    protected void onResume() {
        Log.d("dd","resume");
        super.onResume();
        sensorManager_mag.registerListener(this, sensor_mag, SensorManager.SENSOR_DELAY_NORMAL);
    }


    float magnetic[];
    float[] q_mag_x;
    float[] q_mag_z;
    final int QSIZE = 100;
    final int THROW_SENSORVALUE_INDEX = 20;

    @Override
    public void onSensorChanged(SensorEvent event) {
        System.arraycopy(event.values.clone(), 0, magnetic, 0, event.values.length);//

        textView_x.setText(Float.toString(magnetic[0])); // X
        textView_z.setText(Float.toString(magnetic[1] * (-1)));//event.values[2]; // 반대방향 주의

        if(current_state == 0) // calibration 모드일때
        {
            push_mag_Q(magnetic[0], magnetic[1] * (-1));
            textView_state.setText("Calibration 중입니당");
            if (isFull_mag())//q가 가득 찰때까지 기다린다
            {
                float[] current_mag_x = new float[q_mag_x.length - THROW_SENSORVALUE_INDEX];
                float[] current_mag_z = new float[q_mag_z.length - THROW_SENSORVALUE_INDEX];
                System.arraycopy(q_mag_x, (head_mag + THROW_SENSORVALUE_INDEX) % QSIZE, current_mag_x, 0, q_mag_x.length - THROW_SENSORVALUE_INDEX);
                System.arraycopy(q_mag_z, (head_mag + THROW_SENSORVALUE_INDEX) % QSIZE, current_mag_z, 0, q_mag_z.length - THROW_SENSORVALUE_INDEX);

                m_stateEstimate_x = cal_Average(current_mag_x);
                m_estimateCovariance_x = cal_Variance(current_mag_x, m_stateEstimate_x);
                m_stateEstimate_z = cal_Average(current_mag_z);
                m_estimateCovariance_z = cal_Variance(current_mag_z, m_stateEstimate_z);

                front_x = m_stateEstimate_x;
                front_z = m_stateEstimate_z;
                estimate_x = front_x;//for real kalman

                for (int i = 0; i < current_mag_x.length; i++) {
                    push_noise_Q(current_mag_x[i] - m_stateEstimate_x,
                            current_mag_z[i] - m_stateEstimate_z); // 오류값(추정값 - 실측값)으로 q를 채운다
                }
                noise_stateEstimate_x = cal_Average(q_noise_x);
                noise_estimaeCovariance_x = cal_Variance(q_noise_x, noise_stateEstimate_x);

                noise_stateEstimate_z = cal_Average(q_noise_z);
                noise_estimaeCovariance_z = cal_Variance(q_noise_z, noise_stateEstimate_z);
                //
                current_state = 1;
            }
        }

        if(current_state == 1)
        {
            textView_state.setText("noise est : " + noise_stateEstimate_x + " noise var : " + noise_estimaeCovariance_x +
                    "est : " + m_stateEstimate_x + " var : " + m_estimateCovariance_x);
            //sensorManager_mag.registerListener(this, sensor_mag, SensorManager.SENSOR_DELAY_NORMAL);
            current_state = 3;
        }

        else if(current_state == 3)// DR 모드일때
        {
            textView_state.setText("DR MODE");
            uni_kalmanFilter(magnetic[0], magnetic[1] * (-1)); //추정값 갱신
            kalman_real(magnetic[0]);

            detect_change(magnetic[0], magnetic[1] * (-1));  //방향 탐지 중!

            textView_fx.setText(Float.toString(m_stateEstimate_x));
        }


        addEntry(magnetic[0], m_stateEstimate_x, estimate_x);
    }

    float DETECT_ALARM = 20.0f;
    float HEADDETECT_STAND = 2.0f;
    float HEADDETECT_MOVE = 8.5f;

    void detect_change(float meas_x, float meas_z) {
        float threshold = (float) Math.sqrt((m_stateEstimate_x - meas_x)*(m_stateEstimate_x - meas_x)
                + (m_stateEstimate_z - meas_z)*(m_stateEstimate_z - meas_z));

        if (threshold > DETECT_ALARM) {//recalibration
            //textView_temp.setText("튄 값이 들어와서 calibration");
            flush_Q();
            current_state = 0;
        } else if (((threshold > HEADDETECT_STAND) && (current_state == 1)
                || ((threshold > HEADDETECT_MOVE)  && (current_state == 3))))
        {
            //방향 전환 탐지
            int cal_index = cal_direction(meas_x, meas_z); // 0 : up; 1 :  R 2 : B 3 : R
            if (cal_index >= 0)// 음수값 : 애매하다~
            {
            } else {
                flush_Q();
                current_state = 0;
            }

        }
        else
        {//moving average
            //textView_temp.setText("편 - 안");
        }





    }

    float front_x =0;
    float front_z = 0;

    private int cal_direction(float meas_x, float meas_z) {
        float[] temp = new float[4];
        //root 씌운 값은 구하지 않는다. 왜냐하면 어짜피 min만 알면 되기 때문에. sqrt() 사용 x

        temp[0] = (float) ((front_x - meas_x) * (front_x - meas_x) + (front_z - meas_z) * (front_z - meas_z));//
        temp[1] = (float) ((front_x + meas_z) * (front_x + meas_z) + (front_z - meas_x) * (front_z - meas_x));
        temp[2] = (float) ((front_x + meas_x) * (front_x + meas_x) + (front_z + meas_z) * (front_z + meas_z));//
        temp[3] = (float) ((front_x - meas_z) * (front_x - meas_z) + (front_z + meas_x) * (front_z + meas_x));

        return findMin_with_ambigue(temp);
    }

    float DETECT_AMBIGUOUS = 25.0f;

    int findMin_with_ambigue(float[] input) {
        int index = 0, temp = 1;
        float min = input[0];

        for (int i = 1; i < input.length; i++) {
            if (input[i] < min) {
                temp = index;
                min = input[i];
                index = i;
            }
        }

        //ambiguous
        if (abs(input[temp] - input[index]) < DETECT_AMBIGUOUS)
            return -1;

        return index;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    float estimate_x = 0;
    float var_noise = 1;
    float MEASURE_NOISE = 2f; //가정

    void kalman_real(float m_meas_x)
    {//예측
        //float next_x = estimate_x;
        //float next_var_noise = var_noise;
        //업뎃
        var_noise = var_noise + 10;
        float kal = var_noise / (var_noise + MEASURE_NOISE);
        estimate_x = estimate_x + kal * (m_meas_x - estimate_x);
        var_noise = (1 - kal) * var_noise;

    }


    float m_stateEstimate_z = 0;
    float m_estimateCovariance_z = 0;
    float noise_estimaeCovariance_z = 0;
    float noise_stateEstimate_z = 0;
    float q_noise_x[];
    float q_noise_z[];

    //Return : next state / var , next noise state / var
    void uni_kalmanFilter(float m_meas_x, float m_meas_z)
    //측정 잡음과 예측 잡음 어떻게? = > w는 정규분포(0, 1)를 따르고 u는 구한다
    {
//phase 1 Predict
        float next_stateEstimate = m_stateEstimate_x;// + noise_stateEstimate; // w(n)
        float next_estimateCovariance = m_estimateCovariance_x + noise_estimaeCovariance_x; // w(n)의 분산

        float next_stateEstimate_z = m_stateEstimate_z;// + noise_stateEstimate; // w(n)
        float next_estimateCovariance_z = m_estimateCovariance_z + noise_estimaeCovariance_z; // w(n)의 분산

        //noise update
        push_noise_Q(m_meas_x - m_stateEstimate_x, m_meas_z - m_stateEstimate_z);
        noise_stateEstimate_x = cal_Average(q_noise_x);
        noise_estimaeCovariance_x = cal_Variance(q_noise_x, noise_stateEstimate_x);

        noise_stateEstimate_z = cal_Average(q_noise_z);
        noise_estimaeCovariance_z = cal_Variance(q_noise_z, noise_stateEstimate_z);

        //phase 2 Update
        float innovation = next_stateEstimate - m_meas_x; // yk = inn  ~ -yk
        float innovation_var = next_estimateCovariance + noise_estimaeCovariance_x; // 얼마나 가중치를 줄 것인가... u의 분산 값

        float innovation_z = next_stateEstimate_z - m_meas_z; // yk = inn  ~ -yk
        float innovation_var_z = next_estimateCovariance_z + noise_estimaeCovariance_z;

        //optimal gain computation
        float kFgain = next_estimateCovariance * (1 / innovation_var);
        float kFgain_z = next_estimateCovariance_z * (1 / innovation_var_z);

        //manual gain (1 = full-weight-on-measurement/ 0 = full-weight-on-estimate)
        m_stateEstimate_x = next_stateEstimate - (kFgain * innovation);
        m_estimateCovariance_x = kFgain * noise_estimaeCovariance_x; //Matrix2D.identity is uni-m? => O

        m_stateEstimate_z = next_stateEstimate_z - (kFgain_z * innovation_z);
        m_estimateCovariance_z = kFgain_z * noise_estimaeCovariance_z; //Matrix2D.identity is uni-m? => O
    }

    //Q 둘다 비우기
    private void flush_Q() {
        head_mag = head_noise = rear_mag = rear_noise = 0;
    }

    private boolean isFull_mag() {
        if (((head_mag + 1) % QSIZE) == rear_mag)
            return true;
        return false;
    }

    private boolean isFull_noise() {
        if (((head_noise + 1) % QSIZE) == rear_noise)
            return true;
        return false;
    }

    private void push_mag_Q(float value_x, float value_z) //고치기 필요........
    {
        if (isFull_mag()) {
            //System.out.println("mag Q is full");
            //return; q가 가득차도 그냥 넣는다 ㅎ _ㅎ for moving average
        }
        head_mag = (head_mag + 1) % QSIZE;
        q_mag_x[head_mag] = value_x;
        q_mag_z[head_mag] = value_z;

    }

    private void push_noise_Q(float value_x, float value_z) //고치기 필요........
    {
        if (isFull_noise()) {
            // System.out.println("noise Q is full");
            //return; q가 가득차도 그냥 넣는다 ㅎ _ㅎ for moving average
        }
        head_noise = (head_noise + 1) % QSIZE;
        q_noise_x[head_noise] = value_x;
        q_noise_z[head_noise] = value_z;
    }

    private float cal_Average(float[] input) {
        float sum = 0.0f;

        for (int i = 0; i < input.length; i++) {
            sum = sum + input[i];
        }

        return sum / input.length;
    }

    private float cal_Variance(float[] input, float mean) {
        float sum = 0.0f;
        float temp = 0.0f;

        for (int i = 0; i < input.length; i++) {
            temp = input[i] - mean;
            sum = sum + temp * temp;
        }

        return sum / input.length;
    }
    private void addEntry(float magX, float filteredX, float filteredX2) {

        Log.d("dd","add");

        LineData data = mChart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(set.getEntryCount(), (float) magX), 0);
            data.addEntry(new Entry(set.getEntryCount(), (float) filteredX), 1);
            data.addEntry(new Entry(set.getEntryCount(), (float) filteredX2), 2);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private LineDataSet createSet() {

        Log.d("dd","CreateSet");
        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }
}
